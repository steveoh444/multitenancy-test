# Test - Mutimandantenf�higkeit

## Run
1. Install MSSQL Express <https://www.microsoft.com/de-de/sql-server/sql-server-editions-express>
2. Configure MSSQL Express TCP IP Port and Mixed Mode login with new user test password musterPW1
3. Create the DBs with the [CreateDBs.sql](src/main/resources/db/migration/manual/CreateDBs.sql)
4. mvn clean install
5. java -jar .\targetmultitenancy-test-swarm.jar

## Use
* <http://localhost:8080/>
* Test Show all Mandants from DB as JSON Fromat <http://localhost:8080/rest/testDB>
* Try out the different Mandants 
* <http://localhost:8080/rest/benutzer?mandant=Mandant+1> 
* <http://localhost:8080/rest/benutzer?mandant=Mandant+2>
* Add a Mandant
* <http://localhost:8080/rest/mandanten/add/localhost/test/musterPW1> 

## DB - Mandanten Table Mandant Status

The Mandant Stauts identifies the Status of the Mandant Database given.

| Status  | Meaning  |
|---|---|---
|0  |Mandant is not Ready |
|10 |Mandant is Ready and up|
|255|Mandant is Closed and all DB Connections should close imidiately|

### Soruces
* https://www.tomas-dvorak.cz/posts/jpa-multitenancy/
* https://www.youtube.com/watch?v=5BvJVAlZyvo
* https://wildfly-swarm.gitbooks.io/wildfly-swarm-users-guide/content/common/jpa.html
* https://issues.jboss.org/browse/SWARM-48?jql=text%20~%20%22Injecting%20PersistenceContext%20in%20JAX-RS%22
* http://javapapo.blogspot.de/2015/10/cdi-interceptor-of-http-header-params.html