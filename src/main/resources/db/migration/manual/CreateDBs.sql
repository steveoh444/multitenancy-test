/**
 * Author:  Stefan
 * Created: 30.12.2017
 * Description: Erstellen der Datenbanken
 */

if not exists(select * from sys.databases where name = 'Mandanten')
BEGIN
  CREATE DATABASE [Mandanten]
END
GO

if not exists(select * from sys.databases where name = 'M1')
BEGIN
  CREATE DATABASE [M1]
END
GO

if not exists(select * from sys.databases where name = 'M2')
BEGIN
  CREATE DATABASE [M2]
END
GO
