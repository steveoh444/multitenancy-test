/**
 * Author:  Stefan
 * Created: 30.12.2017
 * Description: Insert für Random 10 Benutzer 
 */

INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (1, N'Leon', N'Bechtholdt')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (2, N'Joseph', N'Curschmann')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (3, N'Silvester', N'Wiesner')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (4, N'Bernd', N'Fried')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (5, N'Erik', N'Brandis')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (6, N'Tobias', N'Mittermeier')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (7, N'Alfred', N'Conzelmann')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (8, N'Samuel', N'Spahn')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (9, N'Dennis', N'Plessner')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (10, N'Andreas', N'Göbel')
GO