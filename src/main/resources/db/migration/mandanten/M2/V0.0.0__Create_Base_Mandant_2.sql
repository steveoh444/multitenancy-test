/**
 * Author:  Stefan
 * Created: 30.12.2017
 * Description: Erstellt eine leere Mandanten Datenbank für Mandant 1
 */

/****** Object:  Table [dbo].[Benutzer]    Script Date: 30.12.2017 16:42:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Benutzer](
	[id] [bigint] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[vorname] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_Benutzer] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO