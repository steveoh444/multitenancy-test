/**
 * Author:  Stefan
 * Created: 30.12.2017
 * Description: Hinzufügen der Mandanten
 */

SET IDENTITY_INSERT [dbo].[Mandanten] ON
GO
INSERT [dbo].[Mandanten] ([id], [name], [jdbcurl], [username], [password], [status]) VALUES (1, N'Mandant 1', N'jdbc:sqlserver://localhost;databaseName=M1', N'test', N'musterPW1', 10)
GO
INSERT [dbo].[Mandanten] ([id], [name], [jdbcurl], [username], [password], [status]) VALUES (2, N'Mandant 2', N'jdbc:sqlserver://localhost;databaseName=M2', N'test', N'musterPW1', 10)
GO
SET IDENTITY_INSERT [dbo].[Mandanten] OFF
GO
