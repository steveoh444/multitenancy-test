/**
 * Author:  Stefan
 * Created: 30.12.2017
 * Description: Erstellt eine Master Mandanten DB 
 */

/****** Object:  Table [dbo].[Mandanten]    Script Date: 30.12.2017 16:26:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mandanten](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](500) NOT NULL,
	[jdbcurl] [nvarchar](1000) NOT NULL,
	[username] [nvarchar](500) NOT NULL,
	[password] [nvarchar](500) NOT NULL,
  [status] [tinyint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

