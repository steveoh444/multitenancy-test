/**
 * Author:  Stefan
 * Created: 30.12.2017
 * Description: Insert für Random 10 Benutzer 
 */

INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (1, N'Maik', N'Waldschmidt')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (2, N'Otto', N'Jonke')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (3, N'Norbert', N'Esser')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (4, N'Leonhard', N'Frey')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (5, N'Maria', N'Krausser')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (6, N'Mario', N'Wieseltier')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (7, N'Justus', N'Florstedt')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (8, N'Günther', N'Braune')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (9, N'Alfons', N'Eisen')
GO
INSERT [dbo].[Benutzer] ([id], [name], [vorname]) VALUES (10, N'Laurens', N'Schneiderman')
GO