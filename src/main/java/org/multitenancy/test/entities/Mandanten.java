package org.multitenancy.test.entities;

import java.io.*;
import javax.persistence.*;

/**
 *
 * @author Stefan
 */
@Entity
@Table(name = "Mandanten")
@NamedQueries(
{
  @NamedQuery(name = "Mandanten.findAll", query = "SELECT m FROM Mandanten m")
  , @NamedQuery(name = "Mandanten.findById", query = "SELECT m FROM Mandanten m WHERE m.id = :id")
  , @NamedQuery(name = "Mandanten.findByName", query = "SELECT m FROM Mandanten m WHERE m.name = :name")
  , @NamedQuery(name = "Mandanten.findByJdbcurl", query = "SELECT m FROM Mandanten m WHERE m.jdbcurl = :jdbcurl")
  , @NamedQuery(name = "Mandanten.findByUsername", query = "SELECT m FROM Mandanten m WHERE m.username = :username")
  , @NamedQuery(name = "Mandanten.findByPassword", query = "SELECT m FROM Mandanten m WHERE m.password = :password")
  , @NamedQuery(name = "Mandanten.findByStatus", query = "SELECT m FROM Mandanten m WHERE m.status = :status")
})
public class Mandanten implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;
  @Basic(optional = false)
  @Column(name = "name")
  private String name;
  @Basic(optional = false)
  @Column(name = "jdbcurl")
  private String jdbcurl;
  @Basic(optional = false)
  @Column(name = "username")
  private String username;
  @Basic(optional = false)
  @Column(name = "password")
  private String password;
  @Basic(optional = false)
  @Column(name = "status")
  private short status;

  public Mandanten()
  {
  }

  public Mandanten(Long id)
  {
    this.id = id;
  }

  public Mandanten(Long id, String name, String jdbcurl, String username, String password, short status)
  {
    this.id = id;
    this.name = name;
    this.jdbcurl = jdbcurl;
    this.username = username;
    this.password = password;
    this.status = status;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getJdbcurl()
  {
    return jdbcurl;
  }

  public void setJdbcurl(String jdbcurl)
  {
    this.jdbcurl = jdbcurl;
  }

  public String getUsername()
  {
    return username;
  }

  public void setUsername(String username)
  {
    this.username = username;
  }

  public String getPassword()
  {
    return password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public short getStatus()
  {
    return status;
  }

  public void setStatus(short status)
  {
    this.status = status;
  }

  @Override
  public int hashCode()
  {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object)
  {
    if (!(object instanceof Mandanten))
    {
      return false;
    }
    Mandanten other = (Mandanten) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
    {
      return false;
    }
    return true;
  }

  @Override
  public String toString()
  {
    return "org.multitenancy.test.entities.Mandanten[ id=" + id + " ]";
  }

}
