package org.multitenancy.test.entities;

import java.io.*;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 *
 * @author Stefan
 */
@Entity
@Table(name = "Benutzer")
@NamedQueries(
        {
          @NamedQuery(name = "Benutzer.findAll", query = "SELECT b FROM Benutzer b")
          , @NamedQuery(name = "Benutzer.findById", query = "SELECT b FROM Benutzer b WHERE b.id = :id")
          , @NamedQuery(name = "Benutzer.findByName", query = "SELECT b FROM Benutzer b WHERE b.name = :name")
          , @NamedQuery(name = "Benutzer.findByVorname", query = "SELECT b FROM Benutzer b WHERE b.vorname = :vorname")
        })
public class Benutzer implements Serializable
{

  private static final long serialVersionUID = 1L;
  @Id
  @Basic(optional = false)
  @NotNull
  @Column(name = "id")
  private Long id;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 255)
  @Column(name = "name")
  private String name;
  @Basic(optional = false)
  @NotNull
  @Size(min = 1, max = 255)
  @Column(name = "vorname")
  private String vorname;

  public Benutzer()
  {
  }

  public Benutzer(Long id)
  {
    this.id = id;
  }

  public Benutzer(Long id, String name, String vorname)
  {
    this.id = id;
    this.name = name;
    this.vorname = vorname;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getVorname()
  {
    return vorname;
  }

  public void setVorname(String vorname)
  {
    this.vorname = vorname;
  }

  @Override
  public int hashCode()
  {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object)
  {
    if (!(object instanceof Benutzer))
    {
      return false;
    }
    Benutzer other = (Benutzer) object;
    if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
    {
      return false;
    }
    return true;
  }

  @Override
  public String toString()
  {
    return "org.multitenancy.test.entities.Benutzer[ id=" + id + " ]";
  }

}
