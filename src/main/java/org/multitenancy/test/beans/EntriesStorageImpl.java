package org.multitenancy.test.beans;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import org.multitenancy.test.entities.*;

/**
 * Entries storage is simple stateless bean without any notion/logic of tenants.
 * The link between tenant and storage is managed purely by EntityManager, which
 * is injected by CDI container and created by
 * {@link cz.tomasdvorak.multitenancy.ProxyEntityManager}.
 */
@Stateless
public class EntriesStorageImpl implements EntriesStorage
{

  /**
   * Injected, tenant aware EntityManager.
   */
  @Inject
  private EntityManager em;

  @Override
  public List<Benutzer> getAllEntries()
  {
    return em.createNamedQuery("Benutzer.findAll").getResultList();
  }
}
