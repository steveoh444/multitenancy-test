package org.multitenancy.test.beans;

import javax.ejb.*;

/**
 * Test for the Scheduler
 * @author Stefan
 */
@Singleton
@Startup
public class TestSchedule
{

  @Schedule(hour = "*", minute = "*", second = "*/3", persistent = false)
  private void checkTenants()
  {
    System.out.println("Test...");
  }
}
