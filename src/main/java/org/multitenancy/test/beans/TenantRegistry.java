package org.multitenancy.test.beans;

import java.sql.*;
import javax.persistence.*;
import java.util.*;
import javax.annotation.*;
import javax.ejb.*;
import javax.transaction.*;
import org.flywaydb.core.*;
import org.flywaydb.core.api.*;
import org.jboss.logging.Logger;
import org.multitenancy.test.entities.*;

/**
 * Loads Tenants from DB, creates EntityManagerFactories for them.
 *
 * TODO: Check for new / deleted Tenants
 */
@Singleton
@Startup
@TransactionManagement(TransactionManagementType.BEAN)
public class TenantRegistry
{

  @PersistenceContext(unitName = "mandanten")
  private EntityManager em;

  @Resource
  UserTransaction tx;

  private String queryDbExist = "SELECT 1 FROM master.sys.databases WHERE [name] = ";

  private final Set<Mandanten> mandaten = new HashSet<>();
  private final Map<String, EntityManagerFactory> entityManagerFactories = new HashMap<>();
  private final Logger LOG = Logger.getLogger(TenantRegistry.class);

  @PostConstruct
  protected void startupTenants()
  {
    final List<Mandanten> mandaten = loadTenantsFromDB();
    LOG.info(String.format("Loaded %d Mandanten from DB.", mandaten.size()));
    mandaten.forEach(mandant ->
    {
      this.mandaten.add(mandant);
      final EntityManagerFactory emf = createEntityManagerFactory(mandant);
      entityManagerFactories.put(mandant.getName(), emf);
      LOG.info("Mandant " + mandant.getName() + " loaded.");
    });
    this.mandaten.addAll(mandaten);
  }

  @PreDestroy
  protected void shutdownTenants()
  {
    entityManagerFactories.forEach((tenantName, entityManagerFactory) -> entityManagerFactory.close());
    entityManagerFactories.clear();
    mandaten.clear();
  }

  private List<Mandanten> loadTenantsFromDB()
  {
    LOG.debug("load Mandanten from DB");
    return em.createNamedQuery("Mandanten.findByStatus")
            .setParameter("status", (short) 10)
            .getResultList();
  }
  
  @Schedule(hour = "*", minute = "*", second = "*/3", persistent = false)
  private void checkTenants(){
    LOG.info("Check for new/del Tenants...");
    System.out.println("Test...");
  }

  /**
   * Create new {@link EntityManagerFactory} using this tenant's schema.
   *
   * @param mandant Tenant used to retrieve schema name
   * @return new EntityManagerFactory
   */
  private EntityManagerFactory createEntityManagerFactory(final Mandanten mandant)
  {
    final Map<String, String> props = new TreeMap<>();
    LOG.info("Creating entity manager factory on jdbc '" + mandant.getJdbcurl() + "' for tenant '" + mandant.getName() + "'.");
    props.put("hibernate.connection.url", mandant.getJdbcurl());
    props.put("hibernate.connection.username", mandant.getUsername());
    props.put("hibernate.connection.password", mandant.getPassword());
    props.put("hibernate.connection.driver_class", "com.microsoft.sqlserver.jdbc.SQLServerDriver");
    props.put("hibernate.show-sql", "true");
    props.put("hibernate.ejb.entitymanager_factory_name", mandant.getId() + mandant.getName());

    return Persistence.createEntityManagerFactory("test", props);
  }

  public Optional<Mandanten> getTenant(final String tenantName)
  {
    return mandaten.stream().filter(tenant -> tenant.getName().equals(tenantName)).findFirst();
  }

  /**
   * Returns EntityManagerFactory from the cache. EMF is created during tenant
   * registration and initialization.
   *
   * @see #startupTenants()
   */
  public EntityManagerFactory getEntityManagerFactory(final String mandantName)
  {
    return entityManagerFactories.get(mandantName);
  }

  /**
   * Looking for the Mandant by Name.
   *
   * @param mandantName
   * @return
   */
  public boolean verifyMandantByName(String mandantName)
  {
    return entityManagerFactories.containsKey(mandantName);
  }

  public void addMandant(
          String jdbcUrlServer,
          String username,
          String password) throws SQLException, FlywayException, ClassNotFoundException, Exception
  {
    try (Connection conn = createNativeDBCon(jdbcUrlServer, username, password))
    {
      Mandanten newMandant = getFreeMandantDBName(jdbcUrlServer, username, password, conn);
      String jdbcUrl = newMandant.getJdbcurl();

      createDatabase(jdbcUrlServer, newMandant.getName(), conn);

      Flyway flyway = new Flyway();
      flyway.setDataSource(jdbcUrl, username, password);
      flyway.setLocations("db/migration/mandanten/M1");
      flyway.migrate();

      newMandant.setStatus((short) 10);

      try
      {
        tx.begin();
        em.merge(newMandant);
        tx.commit();
      }
      catch (Exception e)
      {
        LOG.error("", e);
        try
        {
          tx.rollback();
        }
        catch (IllegalStateException | SecurityException | SystemException ex)
        {
          LOG.error("", ex);
        }
        throw e;
      }
    }
  }

  /**
   * Creates a DB by Raw JDBC to a Database Server given.
   *
   * @param name
   * @param jdbcUrlServer
   * @param username
   * @param password
   * @throws SQLException
   */
  private boolean createDatabase(
          String jdbcUrlServer,
          String dbName,
          Connection conn) throws SQLException
  {
    LOG.info("Creating DB to ADD a Tenant jdbc '" + jdbcUrlServer + "'.");
    try (Statement stmt = conn.createStatement())
    {
      LOG.debug("Creating database...");
      String sql = "CREATE DATABASE " + dbName;
      stmt.executeUpdate(sql);
      LOG.debug("Database created successfully...");
      return true;
    }
  }

  /**
   * Generates a random {@link UUID} and writes the Connection properties to the
   * Mandanten DB to lock the DB Name.
   *
   * @param jdbcUrlServer
   * @param username
   * @param password
   * @return
   */
  private Mandanten getFreeMandantDBName(
          String jdbcUrlServer,
          String username,
          String password,
          Connection conn
  )
  {
    Mandanten mSearch = null;
    String newMandantUuid = null;
    boolean existOnDb = true;
    LOG.debug("Ermittel freien Mandanten DB Namen");
    do
    {
      newMandantUuid = "M_" + UUID.randomUUID().toString().replaceAll("-", "");

      try
      {
        mSearch = (Mandanten) em.createQuery("SELECT m FROM Mandanten m WHERE m.jdbcurl LIKE :search")
                .setParameter("search", "%databaseName=" + newMandantUuid)
                .getSingleResult();
        if (mSearch != null)
        {
          continue;
        }
      }
      catch (NoResultException e)
      {
        // Do nothing
      }
      // Look for DB on the Server 
      try (Statement stmt = conn.createStatement())
      {
        ResultSet rs = stmt.executeQuery(queryDbExist + "'" + newMandantUuid + "'");
        if (!rs.next())
        {
          existOnDb = false;
        }
      }
      catch (SQLException e)
      {
        LOG.error("", e);
      }
    } while (mSearch != null || existOnDb);

    LOG.debug("Neuer mandant lt. " + newMandantUuid);

    LOG.debug("Schreibe neuen Mandanten in Mandantentabelle mit status 0");
    Mandanten mNew = new Mandanten();
    mNew.setName(newMandantUuid);
    mNew.setUsername(username);
    mNew.setPassword(password);
    mNew.setJdbcurl(jdbcUrlServer + ";databaseName=" + newMandantUuid);
    mNew.setStatus((short) 0);
    try
    {
      tx.begin();
      em.persist(mNew);
      tx.commit();
    }
    catch (Exception e)
    {
      LOG.error("", e);
      try
      {
        tx.rollback();
      }
      catch (IllegalStateException | SecurityException | SystemException ex)
      {
        LOG.error("", ex);
      }
    }
    return mNew;
  }

  /**
   * Creating a Native JDBC Connection to a SQL Server.
   *
   * @param jdbcUrlServer
   * @param username
   * @param password
   * @return
   * @throws ClassNotFoundException
   * @throws SQLException
   */
  private Connection createNativeDBCon(String jdbcUrlServer, String username, String password) throws ClassNotFoundException, SQLException
  {

    Connection conn = null;
    //STEP 2: Register JDBC driver
    LOG.debug("Register JDBC Driver");
    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

    //STEP 3: Open a connection
    LOG.debug("Connecting to database...");
    conn = DriverManager.getConnection(jdbcUrlServer, username, password);
    return conn;
  }

}
