package org.multitenancy.test.beans;

import javax.ejb.Local;
import java.util.List;
import org.multitenancy.test.entities.*;

@Local
public interface EntriesStorage
{

  /**
   * Retrieve all messages available in a persistent storage
   */
  List<Benutzer> getAllEntries();
}
