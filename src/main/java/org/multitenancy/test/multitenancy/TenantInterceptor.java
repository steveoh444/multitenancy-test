package org.multitenancy.test.multitenancy;

import javax.inject.Inject;
import javax.interceptor.*;
import javax.servlet.http.*;
import org.jboss.logging.*;
import org.multitenancy.test.beans.*;

/**
 * Wrap every call with tenant identification, detected from list of parameters
 * of called method.
 */
@Interceptor
public class TenantInterceptor
{

  @Inject
  private TenantRegistry tenantRegistry;

  @Inject
  HttpServletRequest servletRequest;

  private final Logger LOG = Logger.getLogger(TenantInterceptor.class);

  @AroundInvoke
  public Object wrapWithTenant(final InvocationContext ctx) throws Exception
  {
    LOG.debug("wrapWithTenant() Called");
    printParameter();
    if (servletRequest.getParameterMap().containsKey("mandant"))
    {
      String mandantNameReq = servletRequest.getParameterMap().get("mandant")[0];
      if (tenantRegistry.verifyMandantByName(mandantNameReq))
      {
        LOG.debug(mandantNameReq + " is verified");
        final String oldValue = TenantHolder.getCurrentTenant();
        LOG.debug("old value " + oldValue);
        try
        {
          TenantHolder.setTenant(mandantNameReq);
          LOG.debug("Mandant gesetzt: " + mandantNameReq);
          return ctx.proceed();
        }
        finally
        {
          if (oldValue != null)
          {
            TenantHolder.setTenant(oldValue);
          }
          else
          {
            TenantHolder.cleanupTenant();
          }
        }
      }
      else
      {
        LOG.error("Mandant not found");
        throw new Exception("Mandant not found");
        //TODO: Response einbauen
//        containerRequestContext.abortWith(
//                Response.status(Response.Status.BAD_REQUEST)
//                        .entity(new ApiError("Mandant not found"))
//                        .type(MediaType.APPLICATION_JSON)
//                        .build());
      }
    }
    else
    {
      LOG.error("Parameter doesnt contain mandant");
      throw new Exception("Parameter doesnt contain mandant");
//      containerRequestContext.abortWith(
//              Response.status(Response.Status.BAD_REQUEST)
//                      .entity(new ApiError("Parameter doesnt contain mandant"))
//                      .type(MediaType.APPLICATION_JSON)
//                      .build());
    }
//    return null;
  }

  private void printParameter()
  {
    if (servletRequest.getParameterMap().isEmpty())
    {
      LOG.debug("No Properties given");
    }
    else
    {
      for (String key : servletRequest.getParameterMap().keySet())
      {
        for (String val : servletRequest.getParameterValues(key))
        {
          LOG.debug(key + "\t" + val);
        }
      }
    }
  }
}
