package org.multitenancy.test.multitenancy;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.lang.reflect.Proxy;
import org.jboss.logging.*;
import org.multitenancy.test.beans.*;

/**
 * This EntityManager producer returns always a Proxy. All the EntityManager
 * methods are wrapped by this proxy. This ensures, that the real EntityManager
 * is obtained/created at call time, not in injection time and can react to
 * Tenant changes between injection and EM method call.
 */
@RequestScoped
public class ProxyEntityManager
{

  private final Logger LOG = Logger.getLogger(ProxyEntityManager.class);

  /**
   * Provider of EntityManagerFactory.
   *
   * @see TenantRegistry#getTenant(String)
   * @see TenantRegistry#createEntityManagerFactory(Tenant)
   */
  @Inject
  private TenantRegistry tenantRegistry;

  /**
   * CDI Producer. Checks if there is a tenant name in ThreadLocal storage
   * {@link TenantHolder}. If yes, load tenant from {@link TenantRegistry}, get
   * its EntityManagerFactory. From the factory create new EntityManager, join
   * JTA transaction and return this EntityManager.
   *
   * @return EntityManager for Tenant or default EntityManager, if no tenant
   * logged in.
   */
  @Produces
  private EntityManager getEntityManager()
  {
    LOG.debug("ProxyEntityManager->getEntityManager() called");
    final String currentTenant = TenantHolder.getCurrentTenant();
    LOG.debug("ProxyEntityManager->getEntityManager() mandant = " + currentTenant);

    final EntityManager target;

    if (currentTenant != null)
    {
      LOG.debug("Returning connection for tenant " + currentTenant);
      target = tenantRegistry.getEntityManagerFactory(currentTenant).createEntityManager();
    }
    else
    {
      target = null;
    }
    return (EntityManager) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class<?>[]
    {
      EntityManager.class
    },
            (proxy, method, args) ->
    {
      target.joinTransaction();
      return method.invoke(target, args);
    });
  }
}
