package org.multitenancy.test.rest;

import java.sql.*;
import java.util.*;
import javax.inject.*;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import org.apache.commons.lang3.exception.*;
import org.flywaydb.core.api.*;
import org.jboss.logging.*;
import org.multitenancy.test.beans.*;
import org.multitenancy.test.dto.*;

@Path("/mandanten")
public class Mandanten
{

  @Inject
  private PersistenceHelper ph;

  @Inject
  private TenantRegistry tenantRegistry;

  private final Logger LOG = Logger.getLogger(TestDB.class);

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/all")
  public List<Mandanten> doGet()
  {
    return ph.getEntityManager().createNamedQuery("Mandanten.findAll").getResultList();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/add/{serverPort}/{username}/{password}")
  public Status doAdd(
          @PathParam("serverPort") String serverPort,
          @PathParam("username") String username,
          @PathParam("password") String password
  )
  {
    try
    {
      tenantRegistry.addMandant("jdbc:sqlserver://" + serverPort, username, password);
      return new Status("OK", "Tenant Added");
    }
    catch (Exception e)
    {
      return new Status("ERROR", "Error during adding Tenant\n" + ExceptionUtils.getStackTrace(e));
    }
  }

}
