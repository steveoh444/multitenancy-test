package org.multitenancy.test.rest;

import javax.enterprise.context.*;
import javax.persistence.*;

/**
 *
 * @author Stefan
 */
@ApplicationScoped
public class PersistenceHelper
{

  @PersistenceContext(unitName = "mandanten")
  private EntityManager em;

  public EntityManager getEntityManager()
  {
    return em;
  }
}
