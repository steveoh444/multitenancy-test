package org.multitenancy.test.rest;

import java.util.*;
import javax.inject.*;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import org.jboss.logging.*;
import org.multitenancy.test.entities.*;

@Path("/testDB")
public class TestDB
{

  @Inject
  private PersistenceHelper ph;

  private final Logger LOG = Logger.getLogger(TestDB.class);

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<Mandanten> doGet()
  {
    LOG.debug("Test");
    return ph.getEntityManager().createNamedQuery("Mandanten.findAll").getResultList();
  }
}
