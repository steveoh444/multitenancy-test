package org.multitenancy.test.rest;

import java.util.*;
import javax.ejb.*;
import javax.inject.*;
import javax.interceptor.*;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import org.jboss.logging.*;
import org.multitenancy.test.beans.*;
import org.multitenancy.test.multitenancy.*;

@Path("/benutzer")
@Stateless
@Interceptors(TenantInterceptor.class)
public class Benutzer
{

  @Inject
  private EntriesStorage storage;

  private final Logger LOG = Logger.getLogger(Benutzer.class);

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<org.multitenancy.test.entities.Benutzer> getAll()
  {
    LOG.debug("getAll() org.multitenancy.test.rest.Benutzer Invoked");
    LOG.debug("MandantName = " + TenantHolder.getCurrentTenant());

    return storage.getAllEntries();
  }
}
