package org.multitenancy.test.rest;

import java.util.*;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

@Path("/hello")
public class HalloEndpoint
{

  @GET
  @Produces("text/plain")
  public Response doGet()
  {
    return Response.ok("method doGet invoked" + new Date()).build();
  }
}
